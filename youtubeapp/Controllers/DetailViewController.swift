//
//  DetailViewController.swift
//  youtubeapp
//

import UIKit
import youtube_ios_player_helper
import FirebaseAuth
import FirebaseFirestore
import EMAlertController

class DetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,DoneLoadDataProtocol,YTPlayerViewDelegate {
    
    var userName = String()
    var dataSetsArray = [DataSets]()
    var userID = String()
    var db = Firestore.firestore()
    var youtubeView = YTPlayerView()
    var searchAndLoad = SearchAndLoadModel()
    var uid = Auth.auth().currentUser?.uid
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        
        searchAndLoad.doneLoadDataProtocol = self
        searchAndLoad.loadMyListData(userName: userName)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSetsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
        cell.titleLabel.text = dataSetsArray[indexPath.row].title
        cell.thumnailImageView.sd_setImage(with: URL(string: dataSetsArray[indexPath.row].url!), completed: nil)
        cell.channelTitleLabel.text = dataSetsArray[indexPath.row].channelTitle
        cell.dateLabel.text = dataSetsArray[indexPath.row].publishTime
        cell.favButton.isHidden = true
        
        return cell
        
    }
    
    func doneLoadData(array: [DataSets]) {
        dataSetsArray = array
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        youtubeView.removeFromSuperview()
        youtubeView.delegate = self
        youtubeView.load(withVideoId: String(dataSetsArray[indexPath.row].videoID!),playerVars: ["playersinline":1])
        youtubeView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(youtubeView)
        
        reLayoutCell()
        
    }
    
    private func reLayoutCell() {
        let topArea = self.view.safeAreaInsets.top
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.tableView)
        
        self.tableView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        self.tableView.heightAnchor.constraint(equalToConstant: topArea + 245).isActive = true
        self.tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: topArea + 245).isActive = true
        
        youtubeView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        youtubeView.heightAnchor.constraint(equalToConstant: 245).isActive = true
        youtubeView.topAnchor.constraint(equalTo: view.topAnchor, constant: topArea).isActive = true
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        
        playerView.playVideo()
        
    }
}
