//
//  SignInViewController.swift
//  youtubeapp
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

class SignInViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    var loading = Loading()
    var message = String()
    var userName = String()
    var db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = nil
        loginButton.layer.cornerRadius = 25
        resetButton.layer.cornerRadius = 25
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signIn" {
            let searchVC = segue.destination as! SearchViewController
            searchVC.userName = userName
        }
    }
    
    private func showAuthAlert(message: String) {
        let alert = UIAlertController(title: "ログインエラー", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func signInAction(_ sender: Any) {
        signInUser()
    }
    
    private func signInUser() {
        
        loading.startAnimation(view: self.view)
        
        guard let email = emailTextfield.text else { return }
        guard let password = passwordTextfield.text else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result,error) in
            
            if let error = error {
                print("ログイン失敗")
                DispatchQueue.main.async{
                    self.loading.stopAnimation()
                }
                
                if let errCode = AuthErrorCode(rawValue: error._code) {
                    switch errCode {
                    case .invalidEmail:
                        self.message = "正しいメールアドレスを入力してください。"
                        self.showAuthAlert(message: self.message)
                        
                    case .userNotFound:
                        self.message = "アカウントが見つかりません。\nアカウントを登録してください。"
                        self.showAuthAlert(message: self.message)
                        
                    case .wrongPassword:
                        self.message = "パスワードが間違っています。"
                        self.showAuthAlert(message: self.message)
                        
                    default:
                        self.message = "認証エラー、もう一度やり直してください。"
                        self.showAuthAlert(message: self.message)
                    }
                }
                return
            }
            print("ログイン成功")
            
            DispatchQueue.main.async{
                self.loading.stopAnimation()
            }
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "searchVC") as! SearchViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
        
        print("ログイン確認中")
        
    }
    
    @IBAction private func resetPassword(_ sender: Any) {
        let resetVC = self.storyboard?.instantiateViewController(withIdentifier: "resetVC") as! ResetViewController
        self.navigationController?.pushViewController(resetVC, animated: true)
    }
}
