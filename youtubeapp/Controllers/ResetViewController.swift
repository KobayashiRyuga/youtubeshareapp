//
//  ResetViewController.swift
//  youtubeapp
//

import UIKit
import  FirebaseAuth

class ResetViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barTintColor = nil
        sendButton.layer.cornerRadius = 25
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: "メール送信完了", message: "入力したメールアドレス宛に、パスワード再設定メールを送信しました。", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func sendEmail() {

        guard let email = emailTextfield.text else { return }
        Auth.auth().languageCode = "ja_JP"
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if let error = error {
                print("再設定メールの送信に失敗")
                return
            }
            print("再設定メール送信")
            self.showAlert()
        }
    }

    @IBAction private func send(_ sender: Any) {
        sendEmail()
    }
    
}
