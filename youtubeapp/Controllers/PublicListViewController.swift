//
//  PublicListViewController.swift
//  youtubeapp
//

import UIKit
import EMAlertController

class PublicListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,DoneLoadUserNameProtocol {
    
    var userNameArray = [String]()
    var searchAndLoad = SearchAndLoadModel()
    var userName = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "UserNameCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        searchAndLoad.doneLoadUserNameProtocol = self
        searchAndLoad.loadOtherListData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "公開リスト"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userNameArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserNameCell
        cell.userNameLabel.text = userNameArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(userNameArray)
        print(userNameArray[indexPath.row])
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        detailVC.userName = userNameArray[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func doneLoadUserName(array: [String]) {
        userNameArray = []
        let orderSet = NSOrderedSet(array: array)
        userNameArray = orderSet.array as! [String]
        userNameArray.sort { $0 < $1 }
        tableView.reloadData()
    }
}
