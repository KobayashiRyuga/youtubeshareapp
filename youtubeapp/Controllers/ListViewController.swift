//
//  ListViewController.swift
//  youtubeapp
//

import UIKit
import SDWebImage
import youtube_ios_player_helper
import FirebaseFirestore

class ListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,DoneLoadDataProtocol,YTPlayerViewDelegate{
    
    var userName = String()
    var dataSetsArray = [DataSets]()
    var searchAndLoad = SearchAndLoadModel()
    var deleteData = DeleteModel()
    var youtubeView = YTPlayerView()
    var db = Firestore.firestore()
    var videoID = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        
        searchAndLoad.doneLoadDataProtocol = self
        searchAndLoad.loadMyListData(userName: userName)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "再生リスト"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSetsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
        cell.titleLabel.text = dataSetsArray[indexPath.row].title
        cell.thumnailImageView.sd_setImage(with: URL(string: dataSetsArray[indexPath.row].url!), completed: nil)
        cell.channelTitleLabel.text = dataSetsArray[indexPath.row].channelTitle
        cell.dateLabel.text = dataSetsArray[indexPath.row].publishTime
        cell.favButton.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let destructiveAction = UIContextualAction(style: .destructive, title: "") { (action, view, completionHandler) in
            self.videoID = self.dataSetsArray[indexPath.row].videoID!
            self.deleteAlert(videoID: self.videoID)
            completionHandler(true)
        }
        destructiveAction.backgroundColor = .systemRed
        destructiveAction.image = UIImage(systemName: "trash.fill")
        let configuration = UISwipeActionsConfiguration(actions: [destructiveAction])
        return configuration
    }
    
    func deleteAlert(videoID: String) {
        let alert = UIAlertController(title: "警告", message: "この動画をリストから削除します。", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.deleteData.deleteVideos(userName: self.userName, videoId: self.videoID)
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    
    func doneLoadData(array: [DataSets]) {
        
        dataSetsArray = array
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        youtubeView.removeFromSuperview()
        
        youtubeView.delegate = self
        youtubeView.load(withVideoId: String(dataSetsArray[indexPath.row].videoID!),playerVars: ["playersinline":1])
        
        reLayoutCell()
    }
    
    private func reLayoutCell() {
        
        let topArea = self.view.safeAreaInsets.top
        
        youtubeView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(youtubeView)
        
        youtubeView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        youtubeView.heightAnchor.constraint(equalToConstant: 245).isActive = true
        youtubeView.topAnchor.constraint(equalTo: view.topAnchor, constant: topArea).isActive = true
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.tableView)
        
        self.tableView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        self.tableView.heightAnchor.constraint(equalToConstant: topArea + 245).isActive = true
        self.tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: topArea + 245).isActive = true
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        
        playerView.playVideo()
        
    }
}
