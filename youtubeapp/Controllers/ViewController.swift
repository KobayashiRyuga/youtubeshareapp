//
//  ViewController.swift
//  youtubeapp
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class ViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    
    var loading = Loading()
    var message = String()
    let db = Firestore.firestore()
    var sendDB = SendDB()
    var check = 0
    var currentUser = Auth.auth().currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = nil
        
        button.layer.cornerRadius = 25
        button.isEnabled = false
        button.alpha = 0.7
        loginButton.layer.cornerRadius = 25
        guestButton.layer.cornerRadius = 25
        
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        nameTextfield.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        check = 0
        emailTextfield.text = ""
        passwordTextfield.text = ""
        nameTextfield.text = ""
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let searchVC = segue.destination as! SearchViewController
        if segue.identifier == "signUp" {
            searchVC.userName = self.nameTextfield.text!
        } else if segue.identifier == "guest" {
            searchVC.uid = nil
        }
    }
    
    @IBAction private func createNewUser(_ sender: Any) {
        checkUserName()
    }
    
    @IBAction func signInUser(_ sender: Any) {
        let signInVC = self.storyboard?.instantiateViewController(withIdentifier: "signInVC") as! SignInViewController
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func usedByGuest(_ sender: Any) {
        self.performSegue(withIdentifier: "guest", sender: nil)
    }
    
    func showAuthAlert(message: String) {
        let alert = UIAlertController(title: "認証エラー", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        if check == 0 {
            present(alert, animated: true, completion: nil)
        }
    }
    
    private func checkUserName() {
        loading.startAnimation(view: self.view)
        guard let userName = nameTextfield.text else { return }
        db.collection("profile").document(userName).addSnapshotListener { (snapShot, error) in
            if error != nil{
                print(error.debugDescription)
                return
            }
            if let data = snapShot?.data() {
                DispatchQueue.main.async{
                    self.loading.stopAnimation()
                }
                if self.check == 0 {
                    self.message = "このユーザー名は使用されています。"
                    self.showAuthAlert(message: self.message)
                }
                return
            } else {
                print("データなし")
                self.createUser()
                return
            }
        }
    }
    
    private func createUser() {
        guard let email = emailTextfield.text else { return }
        guard let password = passwordTextfield.text else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error)  in
            if let error = error {
                
                DispatchQueue.main.async{
                    self.loading.stopAnimation()
                }
                
                if let errCode = AuthErrorCode(rawValue: error._code) {
                    switch errCode {
                    case .invalidEmail:
                        self.message = "正しいメールアドレスを入力してください。"
                        self.showAuthAlert(message: self.message)
                        
                    case .emailAlreadyInUse:
                        self.message = "このメールアドレスは使われています。\nログインするか別のアドレスを使ってください。"
                        self.showAuthAlert(message: self.message)
                        
                    case .weakPassword:
                        self.message = "パスワードは6文字以上で登録してください。"
                        self.showAuthAlert(message: self.message)
                        
                    default:
                        self.message = "認証エラー、もう一度やり直してください。"
                        self.showAuthAlert(message: self.message)
                    }
                }
                return
            }
            print("認証成功")
            
            DispatchQueue.main.async{
                self.loading.stopAnimation()
            }
            self.check = 1
            self.performSegue(withIdentifier: "signUp", sender: nil)
            self.sendAuthMail()
            self.sendUserName()
        }
        print("認証中")
    }
    
    private func sendAuthMail() {
        Auth.auth().languageCode = "ja_JP"
        Auth.auth().currentUser?.sendEmailVerification{ (error) in
            if error != nil {
                return
            }
            print("メール送信")
        }
    }
    
    private func sendUserName() {
        guard let userName = nameTextfield.text else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        sendDB.sendUserName(userName: userName, uid: uid)
    }
}

extension ViewController {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let emailIsEmpty = emailTextfield.text?.isEmpty ?? true
        let passwordIsEmpty = passwordTextfield.text?.isEmpty ?? true
        let nameIsEmpty = nameTextfield.text?.isEmpty ?? true
        
        if emailIsEmpty || passwordIsEmpty || nameIsEmpty {
            button.isEnabled = false
            button.alpha = 0.7
        } else {
            button.isEnabled = true
            button.alpha = 1
        }
    }
    
}

