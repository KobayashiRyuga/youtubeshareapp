//
//  SearchViewController.swift
//  youtubeapp
//

import UIKit
import youtube_ios_player_helper
import FirebaseAuth
import FirebaseFirestore

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,DoneCatchDataProtocol,DoneLoadMyUserNameProtocol,DoneSendDataProtocol,YTPlayerViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var myProfileButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonList: UIStackView!
    @IBOutlet weak var myListButton: UIButton!
    @IBOutlet weak var pubListButton: UIButton!
    
    var dataSetsArray = [DataSets]()
    var userName = String()
    var db = Firestore.firestore()
    var userID = String()
    var searchAndLoad = SearchAndLoadModel()
    var deleteData = DeleteModel()
    var loading = Loading()
    var uid = Auth.auth().currentUser?.uid
    
    var youtubeView = YTPlayerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")

        tableView.separatorColor = .white
        searchTextField.delegate = self
        searchAndLoad.doneLoadMyUserNameProtocol = self

        loadMyUserName()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchTextField.resignFirstResponder()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSetsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
        
        cell.thumnailImageView.sd_setImage(with: URL(string: dataSetsArray[indexPath.row].url!), completed: nil)
        cell.titleLabel.text = dataSetsArray[indexPath.row].title
        cell.channelTitleLabel.text = dataSetsArray[indexPath.row].channelTitle
        cell.dateLabel.text = dataSetsArray[indexPath.row].publishTime
        
        let favButton = cell.favButton
        favButton!.addTarget(self, action: #selector(favButtonTap(_:)), for: .touchUpInside)
        favButton!.tag = indexPath.row
        cell.contentView.addSubview(favButton!)
        
        return cell
    }
    
    @objc private func favButtonTap(_ sender:UIButton){
        if userName != "" {
            let sendDB = SendDB(userID: Auth.auth().currentUser!.uid, userName: userName, urlString: dataSetsArray[sender.tag].url!, videoID: dataSetsArray[sender.tag].videoID!, title: dataSetsArray[sender.tag].title, publishTime: dataSetsArray[sender.tag].publishTime!, description: dataSetsArray[sender.tag].description!, channelTitle: dataSetsArray[sender.tag].channelTitle!)
            sendDB.doneSendDataProtocol = self
            sendDB.sendData(userName: userName)
        } else {
            registerAlert()
        }
    }
    
    func doneSendDataProtocol(sendCheck: Bool) {
        if sendCheck == true {
            let alert = UIAlertController(title: "",message: "再生リストに追加しました。", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        youtubeView.removeFromSuperview()
        
        youtubeView.delegate = self
        
        youtubeView.load(withVideoId: String(dataSetsArray[indexPath.row].videoID!),playerVars: ["playersinline":1])
        
        reLayoutCell()
    }
    
    private func reLayoutCell() {
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.tableView)
        
        self.buttonList.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.buttonList)
        
        youtubeView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(youtubeView)
        
        youtubeView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        youtubeView.heightAnchor.constraint(equalToConstant: 245).isActive = true
        youtubeView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 13).isActive = true
        
        self.tableView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        self.tableView.heightAnchor.constraint(equalToConstant: 503).isActive = true
        self.tableView.topAnchor.constraint(equalTo: youtubeView.bottomAnchor, constant: 0).isActive = true
        
        self.buttonList.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        self.buttonList.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.buttonList.topAnchor.constraint(equalTo: self.tableView.bottomAnchor, constant: 0).isActive = true
        self.buttonList.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func textFieldShouldReturn(_ searchTextField: UITextField) -> Bool {
        loading.startAnimation(view: self.view)
        print(searchTextField.text!)
        
        let urlString = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCjEFo-yGLpcggV4gqIV6K3wq4NBIkBgq8&type=video&part=snippet&q=\(searchTextField.text!)&maxResults=50"
        let searchModel = SearchAndLoadModel(urlString: urlString)
        searchModel.doneCatchDataProtocol = self
        searchModel.search()
        
        searchTextField.resignFirstResponder()
        
        return true
    }
    
    func doneCatchData(array: [DataSets]) {
        print(array.debugDescription)
        dataSetsArray = array
        DispatchQueue.main.async{
            self.loading.stopAnimation()
        }
        tableView.reloadData()
        tableView.scrollToRow(at: .init(row: 0, section: 0), at: .bottom, animated: false)
    }
    
    func loadMyUserName() {
        if uid != nil {
            searchAndLoad.loadMyUserName(uid: uid!)
        }
    }
    
    func doneLoadMyUserNameProtocol(check: Int, userName: String) {
        if check == 1 {
            self.userName = userName
        }
    }
    
    @IBAction private func favList(_ sender: Any) {
        let listVC = self.storyboard?.instantiateViewController(withIdentifier: "listVC") as! ListViewController
        let publicVC = self.storyboard?.instantiateViewController(withIdentifier: "publicVC") as! PublicListViewController
        
        if (sender as AnyObject).tag == 1 {
            if userName != "" {
                listVC.userName = userName
                self.navigationController?.pushViewController(listVC, animated: true)
            } else {
                registerAlert()
            }
        }else{
            self.navigationController?.pushViewController(publicVC, animated: true)
        }
    }
    
    @IBAction private func showMyProfile(_ sender: Any) {
        if userName == "" {
            registerAlert()
        } else {
            let alert = UIAlertController(title: userName, message: "現在\(userName)でサインインしています。", preferredStyle: .alert)
            let signOutAction = UIAlertAction(title: "サインアウト", style: .default, handler: { (_) in
                self.signOutAlert()
            })
            let deleteAction = UIAlertAction(title: "アカウント削除", style: .destructive, handler: {(_) in
                self.deleteAlert()
            })
            let close = UIAlertAction(title: "閉じる", style: .cancel)
            alert.addAction(signOutAction)
            alert.addAction(close)
            alert.addAction(deleteAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    private func registerAlert() {
        let alert  = UIAlertController(title: "ゲスト", message: "現在ゲストで利用しています。プロフィールの閲覧・再生リストへの追加・利用をする場合は、サインアップまたはサインインしてください。", preferredStyle: .alert)
        let signUpAction = UIAlertAction(title: "サインアップまたはサインイン", style: .default, handler: { (_) in
            self.navigationController?.popToRootViewController(animated: true)
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel)
        alert.addAction(signUpAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func signOutAlert() {
        let alert = UIAlertController(title: "サインアウト", message: "サインアウトしてトップに戻ります。", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "はい", style: .default, handler: { (_) in
            self.signOut()
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func signOut() {
        do {
            try Auth.auth().signOut()
            self.navigationController?.popToRootViewController(animated: true)
        } catch let error {
            print("サインアウト失敗")
        }
    }
    
    private func deleteAlert() {
        let alert = UIAlertController(title: "アカウント削除", message: "アカウントを削除してもよろしいですか?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "はい", style: .default, handler: { (_) in
            self.deleteAccount()
        })
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func deleteAccount() {
        Auth.auth().currentUser?.delete() { error in
            if error != nil {
                print(error.debugDescription)
                return
            }
            self.deleteData.deleteData(userName: self.userName, uid: self.uid!)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
