//
//  TrendModel.swift
//  youtubeapp
//
//  Created by 小林竜雅 on 2021/11/04.
//

import Foundation

struct TrendModel{
    
    let videoID:String?
    let title:String?
    let url:String?
    let channelTitle:String?
    let viewCount:String?
    let likeCount:String?
    let disLikeCount:String?
    let description:String?
    let tags:Array<Any>?
    
}
