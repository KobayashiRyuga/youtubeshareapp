//
//  DataSets.swift
//  youtubeapp
//

import Foundation

struct DataSets {
    
    let videoID:String?
    let title:String
    let description:String?
    let url:String?
    let channelTitle:String?
    let publishTime:String?
    
}
