//
//  deleteModel.swift
//  youtubeapp
//

import Foundation
import Firebase
import FirebaseFirestore

class DeleteModel {
    var videoId = String()
    var userName = String()
    var db = Firestore.firestore()
    
    init() {
    }
    
    func deleteVideos(userName:String,videoId:String) {
        db.collection("contents").document(userName).collection("collection").document(videoId).delete { error in
            if error != nil {
                print(error.debugDescription)
                print("データ削除失敗")
            } else {
                print("データ削除成功")
            }
        }
    }
    
    func deleteData(userName: String,uid: String) {
        
        db.collection("contents").document(userName).collection("collection").order(by: "postDate").addSnapshotListener { (snapShot, error) in
            
            if error != nil{
                print(error.debugDescription)
                return
            }
            
            if let snapShotDoc = snapShot?.documents{
                
                for doc in snapShotDoc{
                    
                    let data = doc.data()
                    print(data.debugDescription)
                    if let videoID = data["videoID"] as? String{
                        let batch = self.db.batch()
                        let ref = self.db.collection("contents").document(userName).collection("collection").document(videoID)
                        batch.deleteDocument(ref)
                        batch.commit() { error in
                            if error != nil {
                                print("削除失敗")
                                return
                            }
                            print("削除成功")
            
                        }
                    }
                    
                }
                self.db.collection("collection").document(userName).delete()
                self.db.collection("names").document(uid).delete()
                self.db.collection("profile").document(userName).delete()
            }
            
        }
        
    }
}
