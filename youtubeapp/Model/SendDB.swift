//
//  SendDB.swift
//  youtubeapp
//

import Foundation
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

protocol DoneSendDataProtocol {
    func doneSendDataProtocol(sendCheck: Bool)
}

class SendDB{
    
    var userName = String()
    var imageData = Data()
    var db = Firestore.firestore()
    var doneSendDataProtocol:DoneSendDataProtocol?
    
    var userID = String()
    var urlString = String()
    var videoID = String()
    var publishTime = String()
    var description = String()
    var channelTitle = String()
    var title = String()
    
    init(){
        
    }
    
    init(userID:String,userName:String,urlString:String,videoID:String,title:String,publishTime:String,description:String,channelTitle:String){
        
        self.userID = userID
        self.userName = userName
        self.urlString = urlString
        self.videoID = videoID
        self.title = title
        self.publishTime = publishTime
        self.description = description
        self.channelTitle = channelTitle
        
    }
    
    func sendData(userName:String){
        
        self.db.collection("contents").document(userName).collection("collection").document(videoID).setData(
            ["userID":self.userID as Any,"userName":self.userName as Any,"urlString":self.urlString as Any,"videoID":self.videoID as Any,"title":self.title as Any,"publishTime":self.publishTime as Any,"description":self.description as Any,"channelTitle":self.channelTitle as Any,"postDate":Date().timeIntervalSince1970])
        
        self.doneSendDataProtocol?.doneSendDataProtocol(sendCheck: true)
        
    }
    
    func sendUserName(userName:String, uid:String) {
        self.db.collection("names").document(uid).setData(["userName":userName])
    }
}
