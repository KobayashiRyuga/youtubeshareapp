//
//  SearchAndLoadModel.swift
//  youtubeapp
//

import Foundation
import Firebase
import FirebaseFirestore
import SwiftyJSON
import Alamofire
import FirebaseAuth

protocol DoneCatchDataProtocol {

    func doneCatchData(array:[DataSets])
    
}

protocol DoneLoadNameProtocol {
    
    func doneLoadName(userName:String)
    
}

protocol DoneLoadDataProtocol {
    
    func doneLoadData(array:[DataSets])
    
}

protocol DoneLoadUserNameProtocol{
    
    func doneLoadUserName(array:[String])
    
}

protocol DoneLoadProfileProtocol{
    
    func doneLoadProfileProtocol(check:Int,userName:String,profileTextView:String,imageURLString:String)
    
}

protocol DoneLoadMyUserNameProtocol {
    
    func doneLoadMyUserNameProtocol(check:Int, userName:String)
    
}

class SearchAndLoadModel {
    
    var urlString = String()
    var resultPrePage = Int()
    var dataSetsArray:[DataSets] = []

    var doneCatchDataProtocol:DoneCatchDataProtocol?
    var doneLoadDataProtocol:DoneLoadDataProtocol?
    var doneLoadUserNameProtocol:DoneLoadUserNameProtocol?
    var doneLoadProfileProtocol:DoneLoadProfileProtocol?
    var doneLoadMyUserNameProtocol:DoneLoadMyUserNameProtocol?
    
    var db = Firestore.firestore()
    var userNameArray = [String]()
    
    init(urlString:String) {
        
        self.urlString = urlString
        
    }
    
    init(){
        
    }
    
    func search() {
        
        let encordeUrlString = self.urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        AF.request(encordeUrlString as! URLConvertible, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in
            
            print(response)
            
            switch response.result{
                
            case .success:
                do{
                    let json:JSON = try JSON(data: response.data!)
                    print(json.debugDescription)
                    
                    let totalHitCount = json["pageInfo"]["resultsPerPage"].int
                    if totalHitCount! < 50 {
                        
                        self.resultPrePage = totalHitCount!
                        
                    }else{
                        self.resultPrePage = totalHitCount!
                    }
                    print(self.resultPrePage)
                    for i in 0...self.resultPrePage - 1{
                        
                        if let title = json["items"][i]["snippet"]["title"].string,let description = json["items"][i]["snippet"]["description"].string,let url = json["items"][i]["snippet"]["thumbnails"]["high"]["url"].string,let channelTitle = json["items"][i]["snippet"]["channelTitle"].string,let publishTime = json["items"][i]["snippet"]["publishTime"].string,let channelId = json["items"][i]["snippet"]["channelId"].string{
                            
                            if json["items"][i]["id"]["channelId"].string == channelId{
                            }else{
                                
                                let dataSets = DataSets(videoID: json["items"][i]["id"]["videoId"].string, title: title, description: description, url: url, channelTitle: channelTitle, publishTime: publishTime)
                                
                                if title.contains("Error 404") == true || description.contains("Error 404") == true || url.contains("Error 404") == true || channelTitle.contains("Error 404") == true || publishTime.contains("Error 404") == true{
                                    
                                }else{
                                    
                                    self.dataSetsArray.append(dataSets)
                                    
                                }
                                
                            }
                            
                        }else{
                            print("空です、何か不足しています。")
                        }
                        
                    }
                
                    self.doneCatchDataProtocol?.doneCatchData(array: self.dataSetsArray)
                    
                }catch{
                    
                }
                
            case .failure(_): break
                
            }
            
        }
    }
    
    func loadMyListData(userName:String){
        
        db.collection("contents").document(userName).collection("collection").order(by: "postDate").addSnapshotListener { (snapShot, error) in
            
            self.dataSetsArray = []
            
            if error != nil{
                print(error.debugDescription)
                return
                
            }
            
            if let snapShotDoc = snapShot?.documents{
                
                for doc in snapShotDoc{
                    
                    let data = doc.data()
                    print(data.debugDescription)
                    if let videoID = data["videoID"] as? String,let urlString = data["urlString"] as? String,let publishTime = data["publishTime"] as? String,let title = data["title"] as? String,let description = data["description"] as? String,let channelTitle = data["channelTitle"] as? String{
                        
                        let dataSets = DataSets(videoID: videoID, title: title, description: description, url: urlString, channelTitle: channelTitle, publishTime: publishTime)
                        self.dataSetsArray.append(dataSets)
                        
                    }
                    
                }
             
                self.doneLoadDataProtocol?.doneLoadData(array: self.dataSetsArray)
                
            }
            
        }
        
    }
    
    func loadOtherListData(){
        
        db.collection("names").addSnapshotListener { (snapShot,error) in
            
            if let snapShotDoc = snapShot?.documents{
                
                for doc in snapShotDoc{
                    
                    let data = doc.data()
                    if let userName = data["userName"] as? String{
                        
                        self.userNameArray.append(userName)
                        
                    }
                }
                
                self.doneLoadUserNameProtocol?.doneLoadUserName(array: self.userNameArray)
                
            }
            
        }
        
    }

    func loadMyUserName(uid:String) {
        db.collection("names").document(uid).addSnapshotListener { (snapShot, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            let data = snapShot?.data()
            if let userName = data?["userName"] as? String {
                self.doneLoadMyUserNameProtocol?.doneLoadMyUserNameProtocol(check: 1, userName: userName)
            }
            
        }
    }
}
